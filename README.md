# ePaper

## pinout (top view)

### e-Paper ESP32 Driver Board

```

Arduino LED_BUILTIN: GPIO2

          USB-µB
          +---+
    +-----+   +-----+
CLK | o   +---+   o | 5V
SD0 | o           o | CMD   GPIO11, SD_CMD, SPICS0, HS1_CMD, U1RTS
SD1 | o  ## R     o | SD3   GPIO10, SD_DATA3, SPIWP, HS1_DATA3, U1TXD
P15 | o  ## I     o | SD2   GPIO9,  SD_DATA2, SPIHD, HS1_DATA2, U1RXD
P2  | o  ## B     o | P13   DISPLAY_SCLK, GPIO13, ADC2_CH4, TOUCH4, RTC_GPIO14, MTCK, HSPID,HS2_DATA3, SD_DATA3, EMAC_RX_ER
P0  | o  ## B     o | GND
P4  | o  ## O     o | P12   GPIO12, ADC2_CH5, TOUCH5, RTC_GPIO15, MTDI, HSPIQ,HS2_DATA2, SD_DATA2, EMAC_TXD3
P16 | o  ## N     o | P14   DISPLAY_SDI, GPIO14, ADC2_CH6, TOUCH6, RTC_GPIO16, MTMS, HSPICLK, HS2_CLK, SD_CLK, EMAC_TXD2
P17 | o           o | P27   DISPLAY_DC, GPIO27, ADC2_CH7, TOUCH7, RTC_GPIO17, EMAC_RX_DV
P5  | o           o | P26   DISPLAY_RST, GPIO26, DAC_2, ADC2_CH9, RTC_GPIO7, EMAC_RXD1
P18 | o           o | P25   DISPLAY_BUSY, GPIO25, DAC_1, ADC2_CH8, RTC_GPIO6, EMAC_RXD0
P19 | o           o | P33   GPIO33, XTAL_32K_N, ADC1_CH5, TOUCH8, RTC_GPIO8,  (32.768 kHz crystal oscillator output)
GND | o           o | P32   GPIO32, XTAL_32K_P, ADC1_CH4, TOUCH9, RTC_GPIO9 (32.768 kHz crystal oscillator input)
P21 | o           o | P35   GPI35,  ADC1_CH7, RTC_GPIO5
RX  | o           o | P34   GPI34,  ADC1_CH6, RTC_GPIO4
TX  | o +-------+ o | SVN   GPI39,  SENSOR_VN, ADC1_CH3, ADC_H, RTC_GPIO3
P22 | o | ESP32 | o | SVP   GPI36,  SENSOR_VP, ADC_H, ADC1_CH0, RTC_GPIO0
P23 | o +-------+ o | EN    Chip-enable signal. Active high
GND | o +-------+ o | 3V3
    +---------------+


CLK   GPIO6,  SD_CLK, SPICLK, HS1_CLK, U1CTS
SD0   GPIO7,  SD_DATA0, SPIQ, HS1_DATA0, U2RTS
SD1   GPIO8,  SD_DATA1, SPID, HS1_DATA1, U2CTS
P15   DISPLAY_CS, GPIO15, ADC2_CH3, TOUCH3, MTDO, HSPICS0, RTC_GPIO13, HS2_CMD, SD_CMD, EMAC_RXD3
P2    GPIO2,  ADC2_CH2, TOUCH2, RTC_GPIO12, HSPIWP, HS2_DATA0, SD_DATA0, LED_BUILTIN
P0    GPIO0,  ADC2_CH1, TOUCH1, RTC_GPIO11, CLK_OUT1, EMAC_TX_CLK
P4    GPIO4,  ADC2_CH0, TOUCH0, RTC_GPIO10, HSPIHD, HS2_DATA1, SD_DATA1, EMAC_TX_ER
P16   GPIO16, HS1_DATA4, U2RXD, EMAC_CLK_OUT
P17   GPIO17, HS1_DATA5, U2TXD, EMAC_CLK_OUT_180
P5    GPIO5, VSPICS0, HS1_DATA6, EMAC_RX_CLK
P18   GPIO18, VSPICLK, HS1_DATA7
P19   GPIO19, VSPIQ, U0CTS, EMAC_TXD0
GND 
P21   GPIO21, VSPIHD, EMAC_TX_EN
RX    GPIO3,  U0RXD, CLK_OUT2
TX    GPIO1,  U0TXD, CLK_OUT3, EMAC_RXD2
P22   GPIO22, VSPIWP, U0RTS, EMAC_TXD1
P23   GPIO23, VSPID, HS1_STROBE
GND 
```

### Ribbon Conenctor (top view) PFC24/0.5mm
```
 0 GND

 2 GDR
 3 RESE

 8 BS
 9 IO25 BUSY DISPLAY_BUSY
10 IO26 RST  DISPLAY_RST
11 IO27 D/C  DISPLAY_DC
12 IO15 CS   DISPLAY_CS
13 IO13 SCLK DISPLAY_SCLK
14 IO14 SDI  DISPLAY_SDI
15 3V3 
16 3V3
17 GND

21 PREVGH 
23 PREVGL
```

## Links

- [Board](https://www.waveshare.com/wiki/E-Paper_ESP32_Driver_Board)
- [Board Schemantic](resources/E-Paper_ESP32_Driver_Board_Schematic.pdf)
- [ESP32 Datasheet](resources/ESP32.pdf)
- [GDEW042T2](https://www.bastelgarage.ch/400x300-4-2inch-e-ink-raw-schwarz-weiss-e-paper-display?search=420637)
- [GxEPD](https://github.com/ZinggJM/GxEPD)

```C
// mapping of Waveshare ESP32 Driver Board
// BUSY -> 25, RST -> 26, DC -> 27, CS-> 15, CLK -> 13, DIN -> 14
// NOTE: this board uses "unusual" SPI pins and requires re-mapping of HW SPI to these pins in SPIClass
//       see example GxEPD2_WS_ESP32_Driver.ino, it shows how this can be done easily
```

