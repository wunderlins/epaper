#ifndef _MODEM_H_
#define _MODEM_H_

#include "config.h"

//New transmission method.
//In addition, the gdo0 and gdo2 pin are not required.
//https://github.com/LSatan/SmartRC-CC1101-Driver-Lib
//by Little_S@tan
#include <ELECHOUSE_CC1101_SRC_DRV.h>

int radio_init();
void radio_send_byte(byte b);

#endif