#ifndef _EXAMPLES_H_
#define _EXAMPLES_H_

#include "display.h"
#include <Fonts/FreeMonoBold9pt7b.h>

// comment out unused bitmaps to reduce code space used
#include "bitmaps/Bitmaps200x200.h" // 1.54" b/w
#include "bitmaps/Bitmaps104x212.h" // 2.13" b/w flexible GDEW0213I5F
#include "bitmaps/Bitmaps128x250.h" // 2.13" b/w
#include "bitmaps/Bitmaps128x296.h" // 2.9"  b/w
#include "bitmaps/Bitmaps176x264.h" // 2.7"  b/w
#include "bitmaps/Bitmaps400x300.h" // 4.2"  b/w
#include "bitmaps/Bitmaps640x384.h" // 7.5"  b/w
// 3-color
#include "bitmaps/Bitmaps3c200x200.h" // 1.54" b/w/r
#include "bitmaps/Bitmaps3c104x212.h" // 2.13" b/w/r
#include "bitmaps/Bitmaps3c128x296.h" // 2.9"  b/w/r
#include "bitmaps/Bitmaps3c176x264.h" // 2.7"  b/w/r
#include "bitmaps/Bitmaps3c400x300.h" // 4.2"  b/w/r

// note for partial update window and setPartialWindow() method:
// partial update window size and position is on byte boundary in physical x direction
// the size is increased in setPartialWindow() if x or w are not multiple of 8 for even rotation, y or h for odd rotation
// see also comment in GxEPD2_BW.h, GxEPD2_3C.h or GxEPD2_GFX.h for method setPartialWindow()

void helloWorld();
void helloWorldForDummies();
void helloFullScreenPartialMode();
void helloArduino();
void helloEpaper();
void deepSleepTest();
void showBox(uint16_t x, uint16_t y, uint16_t w, uint16_t h, bool partial);
void drawFont(const char name[], const GFXfont* f);
void showFont(const char name[], const GFXfont* f);
void showPartialUpdate();


#ifdef _GxBitmaps200x200_H_
void drawBitmaps200x200();
#endif

#ifdef _GxBitmaps104x212_H_
void drawBitmaps104x212();
#endif

#ifdef _GxBitmaps128x250_H_
void drawBitmaps128x250();
#endif

#ifdef _GxBitmaps128x296_H_
void drawBitmaps128x296();
#endif

#ifdef _GxBitmaps176x264_H_
void drawBitmaps176x264();
#endif

#ifdef _GxBitmaps400x300_H_
void drawBitmaps400x300();
#endif

#ifdef _GxBitmaps640x384_H_
void drawBitmaps640x384();
#endif

#ifdef _WS_Bitmaps800x600_H_
void drawBitmaps800x600();
#endif

#ifdef _GxBitmaps3c200x200_H_
void drawBitmaps3c200x200();
#endif

#ifdef _GxBitmaps3c104x212_H_
void drawBitmaps3c104x212();
#endif

#ifdef _GxBitmaps3c128x296_H_
void drawBitmaps3c128x296();
#endif

#ifdef _GxBitmaps3c176x264_H_
void drawBitmaps3c176x264();
#endif

#ifdef _GxBitmaps3c400x300_H_
void drawBitmaps3c400x300();
#endif

void drawBitmaps();

#endif