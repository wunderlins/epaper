#ifndef _CONIG_H_
#define _CONIG_H_

/**
 * GPIO config for GxEPD2
 * 
 * Use this to configure the SPI Bus for yoru display. We do not rely 
 * on the standard pin detection of GxEPD2.
 * 
 * You must set the defines before including "display.h".
 * 
 * Some pin laylouts, such as the pin layout of the 
 * "e-Paper ESP32 Driver Board" use a bit strange pin initialisation
 * sequence (see display.cpp/display_init()) on the SPI bus.
 */
// SPI Pins
#define EPD_MISO_PIN 12 // default: 19
#define EPD_SCK_PIN  13 // default: 18
#define EPD_MOSI_PIN 14 // default: 23
#define EPD_CS_PIN   15 // default: 5 (SS)

// display pins
#define EPD_RST_PIN  26
#define EPD_DC_PIN   27
#define EPD_BUSY_PIN 25

// modem CS
// https://forum.micropython.org/viewtopic.php?t=3386#p23668
// SPI1: 6, 8, 7, 11
// HSPI: 14,13,12,15
// VSPI: 18,23,19, 5
#define MODEM_SCK 18
#define MODEM_MOSI 23
#define MDOEM_MISO 19
#define MODEM_CS 5
#define MODEM_GDO0 7
#define MODEM_GDO2 8

#endif