#include <Arduino.h>
#include <GxEPD2_BW.h>
#include "display.h"

// #if defined(ESP32)
GxEPD2_DISPLAY_CLASS<GxEPD2_DRIVER_CLASS, MAX_HEIGHT(GxEPD2_DRIVER_CLASS)>
display(GxEPD2_DRIVER_CLASS(/*CS=*/ EPD_CS_PIN, /*DC=*/ EPD_DC_PIN, /*RST=*/ EPD_RST_PIN, /*BUSY=*/ EPD_BUSY_PIN));
//display(GxEPD2_DRIVER_CLASS(/*CS=*/ 15, /*DC=*/ 27, /*RST=*/ 26, /*BUSY=*/ 25));
//#endif

void display_init() {
  //GxEPD2_DISPLAY_CLASS<GxEPD2_DRIVER_CLASS, MAX_HEIGHT(GxEPD2_DRIVER_CLASS)>
  // display(GxEPD2_DRIVER_CLASS(/*CS=*/ EPD_CS_PIN, /*DC=*/ EPD_DC_PIN, /*RST=*/ EPD_RST_PIN, /*BUSY=*/ EPD_BUSY_PIN));
  
  // uses standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)
  display.init(115200); 
  // *** special handling for Waveshare ESP32 Driver board *** //
  // ********************************************************* //
  SPI.end(); // release standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)
  //SPI: void begin(int8_t sck=-1, int8_t miso=-1, int8_t mosi=-1, int8_t ss=-1);
  // SPI.begin(13, 12, 14, 15); // map and init SPI pins SCK(13), MISO(12), MOSI(14), SS(15)
  
  // map and init SPI pins SCK(13), MISO(12), MOSI(14), SS(15)
  /*
  Serial.println(EPD_SCK_PIN);
  Serial.println(EPD_MISO_PIN);
  Serial.println(EPD_MOSI_PIN);
  Serial.println(EPD_CS_PIN);
  */
  SPI.begin(EPD_SCK_PIN, EPD_MISO_PIN, EPD_MOSI_PIN, EPD_CS_PIN); 
  // *** end of special handling for Waveshare ESP32 Driver board *** //
  // **************************************************************** //

}