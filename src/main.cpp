#include <Arduino.h>

#include "config.h"
#include "display.h"
#include "radio.h"
//#include "examples.h"

/**
 * Hello World Example
 * 
 * an absolute minimal example displaying "Hello World" as text
 * 
 * This should get you started.
 */
#include <Fonts/FreeMonoBold9pt7b.h>
const char HelloWorld[] = "Hello World 1!";
void myHelloWorld()
{
  //Serial.println("helloWorld");
  display.setRotation(1);
  display.setFont(&FreeMonoBold9pt7b);
  display.setTextColor(GxEPD_BLACK);
  int16_t tbx, tby; uint16_t tbw, tbh;
  display.getTextBounds(HelloWorld, 0, 0, &tbx, &tby, &tbw, &tbh);
  // center bounding box by transposition of origin:
  uint16_t x = ((display.width() - tbw) / 2) - tbx;
  uint16_t y = ((display.height() - tbh) / 2) - tby;
  display.setFullWindow();
  display.firstPage();

  Serial.println("");
  int i = 0;
  do
  {
    Serial.print("display refresh number: ");
    Serial.println(i++);
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(x, y);
    display.print(HelloWorld);
  }
  while (display.nextPage());

}

void setup() {

  Serial.begin(115200);
  Serial.println();
  Serial.println("setup");

  delay(50);
  //display_init();
  int init_success = radio_init();
  if (!init_success)
    Serial.println("Failed to initialize Modem");

  // first update should be full refresh
  //myHelloWorld();
  Serial.println("setup done");
  /*
  helloWorld();
  delay(1000);
  // partial refresh mode can be used to full screen,
  // effective if display panel hasFastPartialUpdate

  helloFullScreenPartialMode();
  delay(1000);
  helloArduino();
  delay(1000);
  helloEpaper();
  delay(1000);
  showFont("FreeMonoBold9pt7b", &FreeMonoBold9pt7b);
  delay(1000);
  drawBitmaps();
  if (display.epd2.hasPartialUpdate)
  {
    showPartialUpdate();
    delay(1000);
  } // else // on GDEW0154Z04 only full update available, doesn't look nice
  //drawCornerTest();
  //showBox(16, 16, 48, 32, false);
  //showBox(16, 56, 48, 32, true);
  display.powerOff();
  deepSleepTest();
  Serial.println("setup done");
  */
}

byte b = 1;

void loop() {
  //Transmitt "Hello World" from char format directly.
  //ELECHOUSE_cc1101.SendData("Hello World", 100);
  //delay(2000);

  radio_send_byte(b);
  Serial.println("Sent byte");
  delay(20);
}